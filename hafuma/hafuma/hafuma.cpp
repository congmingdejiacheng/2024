#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<vector>
namespace bit
{
	enum Status
	{
		EMPTY,
		EXIST,
		DELETE
	};
	template<class K,class V>
	struct HashData
	{
		pair<K, V>_kv;
		Status _s;
	};
	template<class K,class V>
	class HashTable
	{
	public:
		HashTable()
		{
			_tables.resize(10);
		}
		bool Insert(const pair<K, V>& kv)
		{
			if (_n * 10 / _tables.size() == 7)
			{
				size_t newSize = _table.size() * 2;
				HashTable<K, V>newHT;
				newHT._tables.resize(newSize);
				//遍历旧表
				for (size_t i = 0; i < _table.size(); i++)
				{
					if (_tables[i]._s == EXIST)
					{
						newHT.Insert(_tables[i]._kv);
					}
				}
				_tables.swap(newHT._tables)
			}
			size_t hashi = kv.first % _tables.size();
			while (_table[hashi].s == EXIST)
			{
				hashi++;
				hashi %= _table.size();
			}
			_tables[hashi].kv = kv;
			_table[hashi].s = EXIST;
			++_n;
		}
	private:
		vector<HashData> _tables;
		size_t _n = 0;//关键字的存储个数；
	};
}