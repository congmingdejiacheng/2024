class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> arr;
        arr[0] = 1;
        int sum = 0, ans = 0;
        int n = nums.size();
        for (int i = 0; i < n; i++)
        {
            sum += nums[i];
            if (arr[sum - k] > 0)
            {
                ans += a rr[sum - k];
            }
            arr[sum]++;
        }
        return ans;

    }
};