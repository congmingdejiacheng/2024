#include <iostream>
#include<vector>
#include<unordered_map>
using namespace std;
int subarraysDivByK(vector<int>& nums, int k) {
    unordered_map<int, int> map;
    map[0] = 1;
    int count = 0;
    int n = nums.size();
    vector<int> sum(n, 0);
    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            sum[i] = nums[i];
            continue;
        }
        sum[i] = sum[i - 1] + nums[i];
    }
    for (auto &a : sum)
    {
        a = (a % k + k) % k;
    }
    for (auto a : sum)
    {
        if (map.count(a))
        {
            count += map[a];
        }
        map[a]++;
    }
    return count;


}
int main()
{
    vector<int> arr = { 4,5,0,-2,-3,1 };
    cout<<subarraysDivByK(arr, 5);
}