#include<bits/stdc++.h>
using namespace std;
const int  N=25;
char g[N][N];
bool st[N][N];
int m,n;
typedef pair<int,int> PII;
#define x first
#define y second
int dx[4]={-1,0,1,0};
int dy[4]={0,1,0,-1};
int  bfs (PII start)
{
    queue<PII> q;
    memset(st,false,sizeof(st));
    q.push(start);
    st[start.x][start.y]=true;
    while(q.size())
    {
    	PII t=q.front();
    	q.pop();
    	for (int i=0;i<4;i++)
    	{
    		int a=start.x+dx[i];
            int b=start.y+dy[i];
    		/*if(st[a][b])
    		continue;
    		if(a<=0||a>n||b<=0||b>n)
    		continue;
    		if(g[a][b]!='.')
    		continue;
    		q.push(make_pair(a,b));
    		st[a][b]=true;*/
    		if (a >= 0 && a < n && b >= 0 && b < m && g[a][b] != '#' && st[a][b] == false)
            {
                st[a][b] = true;
                q.push(make_pair(a, b));
            }
		}
	}
	int cnt=0;
	for (int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			if(st[i][j])
			{
				cnt++;
			}
		}
	}
	return cnt;
     
}
int main ()
{
	int a=0;
	while(scanf("%d%d",&m,&n),n||m)
	{
	  for(int i=0;i<n;i++)
	  {
		for(int j=0;j<m;j++)
		{
			cin>>g[i][j];
		}
	 }
	 
	
	PII start;
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			if(g[i][j]=='@')
			{
				start.x=i;
				start.y=j;
			}
		}
	}

	cout<< bfs(start)<<endl;
   }
	
	
	return 0;
 } 
