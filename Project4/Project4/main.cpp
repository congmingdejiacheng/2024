#include<iostream>
using namespace std;

class A
{
public:
	virtual void f() {}

	int _a1 = 1;
};

class B : public A
{
public:
	int _b1 = 1;
};

//void fun(A* pa)
//{
//	// 无差别转换，存在一定风险
//	B* pb = (B*)pa;
//	cout << pb << endl;
//	pb->_b1++;
//}

void fun(A* pa)
{
	// pa指向B对象，转换成功
	// pa指向A对象，转换失败，返回空

	B* pb = dynamic_cast<B*>(pa);
	if (pb)
	{
		cout << pb << endl;
		pb->_b1++;
	}
	else
	{
		cout << "转换失败" << endl;
	}
}

//int main()
//{
//	A a;
//	B b;
//	fun(&a);
//	fun(&b);
//
//	return 0;
//}


//int main()
//{
//	/*cout << "xxxx" << endl;
//	cerr << "yyyy" << endl;*/
//	int x = 0;
//	cout << cin.good() << endl;
//	cout << cin.eof() << endl;
//	cout << cin.fail() << endl;
//	cout << cin.bad() << endl << endl;
//
//	cin >> x;
//	cout << x << endl;
//
//	cout << cin.good() << endl;
//	cout << cin.eof() << endl;
//	cout << cin.fail() << endl;
//	cout << cin.bad() << endl << endl;
//
//	cin.clear();
//	cin.get();
//	
//	cout << cin.good() << endl;
//	cout << cin.eof() << endl;
//	cout << cin.fail() << endl;
//	cout << cin.bad() << endl << endl;
//
//	cin >> x;
//	cout << x << endl;
//
//	return 0;
//}

//int main()
//{
//	// 在io需求比较高的地方，如部分大量输入的竞赛题中，加上以下3行代码
//	// 可以提高C++IO效率
//	ios_base::sync_with_stdio(false);
//	cin.tie(nullptr);
//	cout.tie(nullptr);
//
//	/*int x = 0;
//	cout << "请输入：";
//	cin >> x;*/
//
//	//printf("xxxx");
//	//cout << "yyyy" << endl;
//	//printf("\n");
//
//	return 0;
//}

#include <fstream>      // std::ofstream

//int main() 
//{
//	std::ofstream ofs("test.txt", ofstream::out | ofstream::app);
//	ofs << "xxxxxxxxxxxxxx";
//
//	return 0;
//}

//int main() {
//
//	ifstream ifs("D:\\360MoveData\\Users\\xjh\\Desktop\\7-25-上午.png", ifstream::in | ifstream::binary);
//	char c = ifs.get();
//	int n = 0;
//	while (ifs.good()) {
//		++n;
//		//cout << c;
//		c = ifs.get();
//	}
//	cout <<n<<endl;
//
//	cout << ifs.good() << endl;
//	cout << ifs.eof() << endl;
//	cout << ifs.fail() << endl;
//	cout << ifs.bad() << endl << endl;
//
//	return 0;
//}

class Date
{
	friend ostream& operator << (ostream& out, const Date& d);
	friend istream& operator >> (istream& in, Date& d);
public:
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}
private:
	int _year;
	int _month;
	int _day;
};

istream& operator >> (istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}

ostream& operator << (ostream& out, const Date& d)
{
	out << d._year << " " << d._month << " " << d._day;
	return out;
}

struct ServerInfo
{
	//char _ip[32];
	string _ip;
	int _port;
	Date _date;
};

struct ConfigManager
{
public:
	ConfigManager(const char* filename = "test.txt")
		:_filename(filename)
	{}

	void WriteBin(const ServerInfo& info)
	{
		ofstream ofs(_filename, ios_base::out | ios_base::binary);
		ofs.write((const char*)&info, sizeof(ServerInfo));
	}

	void ReadBin(ServerInfo& info)
	{
		ifstream ifs(_filename, ios_base::in | ios_base::binary);
		ifs.read((char*)&info, sizeof(info));
	}

	void WriteText(const ServerInfo& info)
	{
		ofstream ofs(_filename);
		ofs << info._ip << " " << info._port << " " << info._date;
	}

	void ReadText(ServerInfo& info)
	{
		ifstream ifs(_filename);
		ifs >> info._ip >> info._port >> info._date;
	}

private:
	string _filename; // 配置文件
};

//int main()
//{
//	ServerInfo winfo = { "192.0.0.1111111111111111111111111111", 80, { 2022, 4, 10 } };
//	// 二进制读写
//	ConfigManager cf_bin("test.bin");
//	cf_bin.WriteBin(winfo);
//
//	return 0;
//}

//int main()
//{
//	ConfigManager cf_bin("test.bin");
//	ServerInfo rbinfo;
//	cf_bin.ReadBin(rbinfo);
//
//	cout << rbinfo._ip << " " << rbinfo._port << " " << rbinfo._date << endl;
//
//	return 0;
//}

//int main()
//{
//	ServerInfo winfo = { "192.0.0.1xxxxxxxxxxxx", 80, { 2022, 4, 10 } };
//
//	// 文本读写
//	ConfigManager cf_text("test.text");
//	cf_text.WriteText(winfo);
//
//	return 0;
//}

//int main()
//{
//	ConfigManager cf_text("test.text");
//	ServerInfo rtinfo;
//	cf_text.ReadText(rtinfo);
//	cout << rtinfo._ip << " " << rtinfo._port << " " << rtinfo._date << endl;
//
//	return 0;
//}

#include<sstream>

struct ChatInfo
{
	string _name; // 名字
	int _id;      // id
	Date _date;   // 时间
	string _msg;  // 聊天信息
};

//int main()
//{
//	// 结构信息序列化为字符串
//	ChatInfo winfo = { "张三", 135246, { 2022, 4, 10 }, "晚上一起看电影吧" };
//	ostringstream oss;
//	oss << winfo._name << " " << winfo._id << " " << winfo._date << " " << winfo._msg;
//	string str = oss.str();
//
//	//send(str.c_str(), str.size());
//
//	cout << str << endl << endl;
//
//	// recv
//	// 字符串解析成结构信息
//	ChatInfo rInfo;
//	istringstream iss;
//	iss.str(str);
//	iss >> rInfo._name >> rInfo._id >> rInfo._date >> rInfo._msg;
//	cout << "-------------------------------------------------------" << endl;
//	cout << "姓名：" << rInfo._name << "(" << rInfo._id << ") ";
//	cout << rInfo._date << endl;
//	cout << rInfo._name << ":>" << rInfo._msg << endl;
//	cout << "-------------------------------------------------------" << endl;
//
//	return 0;
//}

//int main()
//{
//	// 结构信息序列化为字符串
//	ChatInfo winfo = { "张三", 135246, { 2022, 4, 10 }, "晚上一起看电影吧" };
//	stringstream oss;
//	oss << winfo._name << " " << winfo._id << " " << winfo._date << " " << winfo._msg;
//	string str = oss.str();
//
//	//send(str.c_str(), str.size());
//
//	cout << str << endl << endl;
//
//	// recv
//	// 字符串解析成结构信息
//	ChatInfo rInfo;
//	stringstream iss;
//	iss.str(str);
//	iss >> rInfo._name >> rInfo._id >> rInfo._date >> rInfo._msg;
//	cout << "-------------------------------------------------------" << endl;
//	cout << "姓名：" << rInfo._name << "(" << rInfo._id << ") ";
//	cout << rInfo._date << endl;
//	cout << rInfo._name << ":>" << rInfo._msg << endl;
//	cout << "-------------------------------------------------------" << endl;
//
//	return 0;
//}