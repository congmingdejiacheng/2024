class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int right = 0;
        int left = 0;
        int sum = 0;
        int len = INT_MAX;
        int n = nums.size();
        for (; right < n; right++)
        {
            sum += nums[right];
            while (sum >= target)
            {
                len = min(len, right - left + 1);
                sum -= nums[left++];
            }
        }
        return len == INT_MAX ? 0 : len;

    }
};
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> arr;
        if (nums.size() < 4)
        {
            return arr;
        }
        sort(nums.begin(), nums.end());
        int N = nums.size();
        for (int i = 0; i < N - 3;)
        {
            for (int j = i + 1; j < N - 2;)
            {
                int m = j + 1;
                int n = N - 1;
                while (m < n)
                {
                    if ((long)nums[i] + nums[j] == (long)target - (nums[m] + nums[n]))
                    {
                        arr.push_back({ nums[i],nums[j],nums[m],nums[n] });
                        m++;
                        n--;
                        while (nums[m] == nums[m - 1] && m < n)
                            m++;
                        while (nums[n] == nums[n + 1] && m < n)
                            n--;
                    }
                    else if ((long)nums[i] + nums[j] > (long)target - (nums[m] + nums[n]))
                        n--;
                    else
                        m++;

                }

                j++;
                while (nums[j] == nums[j - 1] && j < N - 2)
                {
                    j++;
                }
            }
            i++;
            while (nums[i] == nums[i - 1] && i < N - 3)
            {
                i++;
            }
        }
        return arr;
    }


};