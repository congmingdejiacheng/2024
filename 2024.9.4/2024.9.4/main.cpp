#include<iostream>
#include<string>
using namespace std;
    bool isUnique(string astr) {
        int c = sizeof(astr);
        int d = astr.size();
        if (sizeof(astr)-1 > 26)
            return false;
        int bitmap = 0;
        for (auto a : astr)
        {
            int i = a - 'a';
            if ((bitmap >> i) & 1)
            {
                return false;
            }
            else
                bitmap |= (1 << i);
        }
        return true;

    }

int main()
{
	string s = { "abc"};
    isUnique(s);
	return 0;
}