class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums)
    {
        int first = 0;
        int second = first + 1;
        int third = nums.size() - 1;
        sort(nums.begin(), nums.end());
        vector<vector<int>> vv;

        while (first <= nums.size() - 3)
        {
            if (nums[first] > 0) break;
            second = first + 1;   third = nums.size() - 1;
            while (second < third)
            {

                if (nums[first] + nums[second] + nums[third] < 0)
                {
                    second++;
                }
                else if (nums[first] + nums[second] + nums[third] == 0)
                {
                    //vector<int> v;
                   // v.push_back(nums[first]);
                    //v.push_back(nums[second]);
                    //v.push_back(nums[third]);
                    vv.push_back({ nums[first],nums[second],nums[third] });
                    third--;
                    second++;
                    //去重
                    while (second < third && nums[second] == nums[second - 1])
                    {
                        second++;
                    }
                    while (third > second && (nums[third] == nums[third + 1]))
                    {
                        third--;
                    }
                }
                else
                {
                    third--;
                }

            }
            first++;

            while (nums[first] == nums[first - 1] && first <= nums.size() - 3)
            {
                first++;
            }



        }
        return vv;

    }
};
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums)
    {
        vector<vector<int>>arr1;

        int n = nums.size();
        sort(nums.begin(), nums.end());
        for (int i = 0; i <= n - 3;)
        {
            if (nums[i] > 0)break;
            int j = i + 1;
            int k = n - 1;
            while (j < k)
            {
                if (nums[i] + nums[j] + nums[k] == 0)
                {
                    //arr2.push_back(nums[i]);
                    //arr2.push_back(nums[j]);
                    //arr2.push_back(nums[k]);
                    arr1.push_back({ nums[i],nums[j],nums[k] });

                    j++;
                    k--;
                    while (j < k && nums[j] == nums[j - 1])
                    {
                        j++;
                    }
                    while (j < k && nums[k] == nums[k + 1])
                    {
                        k--;
                    }
                }
                else if (nums[i] + nums[j] + nums[k] > 0)
                {
                    k--;
                }
                else
                {
                    j++;
                }


            }
            i++;
            while (nums[i] == nums[i - 1] && i <= n - 3)//这个不可以放在最前面；原因0前面的-1有越界；
            {
                i++;
            }
        }
        return arr1;
    }
};