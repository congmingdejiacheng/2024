//#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//#include<cstdio>
//#include<algorithm>
//#include<cstring>
//#include<vector>
//using namespace std;
//const int N = 60;
//int arr[N][N];
//int n, m, k;
//int ans = 0;
//vector <int> arr1;
//int cir(int i, int j, int k)
//{
//    if ((i == n + 1 && j == m && ans <= k) || (i == n && j == m + 1 && ans <= k))
//    {
//        if (ans == k)
//        {
//            return 1;
//        }
//        else
//        {
//            return 0;
//        }
//    }
//    if (i > n || j > m || ans > k)
//    {
//        return 0;
//    }
//
//    int b = 0;
//    arr1.push_back(0);
//    int x =arr1.back();
//    if (x >= arr[i][j])
//            b = 1;
//    
//    int a = 0;
//    if (b == 0)
//    {
//        arr1.push_back(arr[i][j]);
//        ans++;
//        a = cir(i + 1, j, k) + cir(i, j + 1, k);
//        arr1.pop_back();
//        ans--;
//        a += cir(i + 1, j, k) + cir(i, j + 1, k);
//    }
//    else
//    {
//        a += cir(i + 1, j, k) + cir(i, j + 1, k);
//    }
//    return a;
//
//}
//int main()
//{
//
//    cin >> n >> m >> k;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> arr[i][j];
//        }
//    }
//    int w = cir(1, 1, k) >> 1;
//    cout << w % 1000000007-4;
//    return 0;
//}
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
const int N = 60;
int arr[N][N];
int n, m, k;
int ans = 0;
vector <int> arr1;
int cir(int i, int j, int k)
{
    if ((i == n + 1 && j == m && ans <= k) || (i == n && j == m + 1 && ans <= k))
    {
        if (ans == k)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    if (i > n || j > m || ans > k)
    {
        return 0;
    }

    int b = 0;


    for (auto x : arr1)
    {
        if (x >= arr[i][j])
            b = 1;
    }

    int a = 0;
    if (b == 0)
    {
        arr1.push_back(arr[i][j]);
        ans++;
        a = cir(i + 1, j, k) + cir(i, j + 1, k);
        arr1.pop_back();
        ans--;
        a += cir(i + 1, j, k) + cir(i, j + 1, k);
    }
    else
    {
        a += cir(i + 1, j, k) + cir(i, j + 1, k);
    }
    return a;

}
int main()
{

    cin >> n >> m >> k;
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            cin >> arr[i][j];
        }
    }
    int w = cir(1, 1, k) >> 1;
    cout << w % 1000000007;
    return 0;
}