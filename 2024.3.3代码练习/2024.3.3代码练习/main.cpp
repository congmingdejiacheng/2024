#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int N = 100010;
int a[N];
int s[N];
int n, k;
int c = 0;
int main()
{
    cin >> n >> k;
    for (int i = 1; i <= n; i++)
    {
        cin >> a[i];
        s[i] = s[i - 1] + a[i];
    }
    int sum = 0;
    for (int i = n; i > 0; i--)
    {
        for (int j = i - 1; j >= 0; j--)
        {
            sum = s[i] - s[j];
            if (sum % k == 0)
            {
                c++;
            }
        }
    }
    printf("%d", c);
    return 0;
}