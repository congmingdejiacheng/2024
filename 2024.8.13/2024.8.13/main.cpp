class Solution {
public:
    int takeAttendance(vector<int>& records) {
        int n = records.size();
        int left = 0, right = n - 1;
        while (left < right)
        {
            int mid = (left + right) / 2;
            if (records[mid] > mid)
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }

        }
        if (records[left] == left)
            return left + 1;
        return left;

    }
};
class Solution {
public:
    int findMin(vector<int>& nums) {
        int n = nums.size();
        int left = 0, right = n - 1;
        while (left < right)
        {
            int mid = (left + right) / 2;
            if (nums[mid] <= nums[n - 1])
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }
        }
        return nums[left];

    }
};