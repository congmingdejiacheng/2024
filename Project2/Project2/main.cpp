#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <unordered_map>

int main() {
    // 创建一个 unordered_map
    std::unordered_map<std::string, int> myMap;

    // 插入键值对
    myMap["apple"] = 10;
    myMap["orange"] = 7;
    myMap["banana"] = 5;

    // 访问元素
    std::cout << "Number of apples: " << myMap["apple"] << std::endl;

    // 遍历哈希表
    for (const auto& pair : myMap) {
        std::cout << pair.first << ": " << pair.second << std::endl;
    }

    // 检查某个键是否存在
    if (myMap.count("banana") > 0) {
        std::cout << "Banana exists in the map" << std::endl;
    }

    // 删除某个键值对
    myMap.erase("orange");

    // 清空哈希表
    myMap.clear();

    return 0;
}