class Solution {
public:
    void swap(char& a, char& b) {            // (1)
        char tmp = a;
        a = b;
        b = tmp;
    }
    void reverseString(vector<char>& s) {
        int len = s.size();
        for (int i = 0; i < len / 2; ++i) {   // (2)
            swap(s[i], s[len - i - 1]);
        }
    }
};
