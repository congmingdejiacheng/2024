#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//#include<vector>
//using namespace std;
//#include"BRtree.h"
//
//
//
//int main()
//{
//	const int N = 1000;
//	vector<int>v;
//	v.reserve(N);
//	for (size_t i = 0; i < N; i++)
//	{
//		v.push_back(rand() + i);
//	}
//	size_t begin2 = clock();
//	RBTree<int, int>t;
//
//	for (auto e : v)
//	{
//		t.Insert(make_pair(e, e));
//		cout << "Inserrt:" << e << "->" << t.IsBalance() << endl;
//
//	}
//	size_t end2 = clock();
//	cout << "Insert:" << end2 - begin2 << endl;
//	cout << t.IsBalance() << endl;
//	return 0;
//}
#include<iostream>
#include<vector>
using namespace std;

#include"BRTree.h"
#include"map.h"
#include"set.h"
//void test_map()
//{
//	bit::map<string, string> dict;
//	dict.insert(make_pair("sort", ""));
//	dict.insert(make_pair("sort", "xx"));
//	dict.insert(make_pair("left", ""));
//	dict.insert(make_pair("right", "?"));
//
//	bit::map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		// key?
//		//it->first += 'x';
//		it->second += 'y';
//
//		cout << it->first << ":" << it->second << endl;
//		++it;
//	}
//	cout << endl;
//
//	string arr[] = { "?", "","?", "", "?", "", "?", "?", "", "?", "?", "?", "?" };
//	bit::map<string, int> countMap;
//	for (auto& e : arr)
//	{
//		countMap[e]++;
//	}
//
//	for (auto& kv : countMap)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//}


void test_set()
{
	bit::set<int> s;
	s.insert(4);
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(2);
	s.insert(0);
	s.insert(10);
	s.insert(5);

	bit::set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		//*it += 1;

		cout << *it << " ";
		++it;
	}
	cout << endl;

	// key?
	it = s.begin();
	// *it = 100;

	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}
int main()
{
	/*test_map();*/
	test_set();

	return 0;
}