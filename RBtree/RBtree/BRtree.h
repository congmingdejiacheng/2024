#pragma once
#include<assert.h>
#include<utility>
enum Colour
{
	RED,
    BLACK
};
template <class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T _kv;
	Colour _col;
	RBTreeNode(const T& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _col(RED)
	{}
};
template<class T,class Ref ,class Ptr>//ref是引用，ptr是指针
struct _TreeIterator
{   
	typedef RBTreeNode<T> Node;
	typedef _TreeIterator <T, Ref, Ptr>Self;
	Node* _node;
	_TreeIterator(Node* node)
		:_node(node)
	{}
	Ref operator* ()
	{
		return _node->_kv;
	}
	Ptr operator ->()
	{
		return &_node->_kv;
	}
	/*++规律
		1.it指向的结点，右子树不为空，下一个就是右子树的最左结点；
		2.it指向的结点，右子树为空，则it的结点所在的右子树放问完了，往上找某个结点是其父节点的左结点；*/
	Self& operator++()
	{
		if (_node->_right)
		{
			Node* cur = _node->_right;
			while (cur->_left)
			{
				cur = cur->_left;
			}
			_node = cur;
		}
		else
		{
			Node* cur = _node;
			Node* parent = _node->_parent;
			while (parent && cur == parent->_right)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	Self operator --()
	{
		if (_node->left)
		{
			Node* cur = _node->_left;
			while (cur->_right)
			{
				cur = cur->_right;
			}
			_node = cur;

		}
		else
		{
			Node* cur = _node;
			Node* parent = _node->_parent;
			while (parent && cur == parent->_left)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	bool operator !=(const Self& s)
	{
		return _node != s._node;
	}
	bool operator ==(const Self& s)
	{
		return _node == s._node;
	}

};

template<class K,class T,class KeyOFT>//这里的KeyOFT是仿函数；用map封装时间的比较大小比较
class RBTree
{

	typedef RBTreeNode<T> Node;
public:
	typedef _TreeIterator<T, T&, T*> iterator;
	typedef _TreeIterator<T, const T&, const T*>const_iterator;
	iterator begin()
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}
		return iterator(cur);
	}
	iterator end()
	{
		return iterator(nullptr);
	}
	const_iterator begin()const
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}
		return const_iterator(cur);
	}
	const_iterator end()const
	{
		return const_iterator(nullptr);
	}
	pair<Node*,bool> Insert(const T& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return make_pair(_root,true);
		}
		Node* parent = nullptr;
		Node* cur = _root;
		KeyOFT kot;//这里使用仿函数就是将每个结点的首元素取出来；仿函数是需要定义一遍才可以使用，原因发仿函数本质是类
		while (cur)
		{
			if (kot(cur->_kv) < kot(kv))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if(kot(cur->_kv)>kot(kv))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(cur,false);
			}
		}
		cur = new Node(kv);
		Node* newnode = cur;
		cur->_col = RED;
		if (kot(parent->_kv) > kot(kv))
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			if (parent==grandfather->_left)
			{
				//       g
				//   p       u
				//c
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandfather->_col = RED;
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_left)
					{
						//单旋
						//       g
						//     p   u
						//  c
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;

					}
					else
					{
						//双旋
						//        g
						//     p     u
						//        c
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			else
			{
				//    g
				//  u   p
				//        c
				Node* uncle = grandfather->_left;
				if (uncle&&uncle->_col==RED)
				{
					grandfather->_col = RED;
					uncle->_col = parent->_col = BLACK;
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_right)
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}

		}
		//这里是为了解决情况一
		_root->_col = BLACK;
		return make_pair(newnode,true);

	}

	//左单旋转
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		subR->_left = parent;
		Node* parentParent = parent->_parent;
		parent->_parent = subR;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = subR;
			}
			else
			{
				parentParent->_right = subR;
			}
			subR->_parent = parentParent;
		}
	}


	//右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node* parentParent = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (_root == parent)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = subL;
			}
			else
			{
				parentParent->_right = subL;
			}

			subL->_parent = parentParent;
		}
	}
	bool check(Node* root, int blacknum, const int refVal)
	{
		//使用递归来判断是否存在连续红节点；和每条支路的黑色结点数目相同
		if (root == nullptr)//到达尾结点
		{
			if (blacknum != refVal)
			{
				cout << "存在黑结点数目不相等的路径" << endl;
				return false;
			}
			return true;
		}
		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "有连续的红色结点" << endl;
			return false;
		}
		if (root->_col == BLACK)
		{
			++blacknum;
		}
		return check(root->_left, blacknum, refVal)
			&& check(root->_right, blacknum, refVal);

	}
	
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	void _InOrder(Node* root)

	{
	
			if (root == nullptr)
			{
				return;
			}
			_InOrder(root->_left);
			cout << root->_kv.first << " ";
			_InOrder(root->_right);
		
	}
	bool IsBalance()
	{
		if (_root == nullptr)
		{
			return true;
		}
		if (_root->_col == RED)
		{
			return false;
		}
		int refval = 0;//参考值
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
			{
				++refval;
			}
			cur = cur->_left;
		}//将一条支路作为参考的黑结点个数的参考值；
		int blacknum = 0;
		return check(_root, blacknum, refval);
	}
	int Height()
	{
		return _Height(_root);
	}
	int _Height(Node* root)
	{
		if (root == nullptr)
		{
			return 0;
		}
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;

	}
	size_t Size()
	{
		return _Size(_root);
	}
	size_t _Size(Node* root)
	{
		if (root == NULL)
		{
			return 0;
		}
		return _Size(root->_left) + _Size(root->_right) + 1;
	}
	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (cur->_kv.first < key)
			{
				cur = cur->_right;
			}
			else if (cur->_kv.first > key)
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return NULL;
	}

private:
	Node* _root = nullptr;
};


























