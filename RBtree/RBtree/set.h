#pragma once
#pragma once
#include"BRtree.h"
namespace bit
{
	template<class K>
	class set
	{
	public:
		struct SetKeyOfT
		{
			const K& operator()(const K& kv)
			{
				return kv;
			}
		};
		typedef typename RBTree<K, K, SetKeyOfT>::const_iterator iterator;
		typedef typename RBTree<K, K, SetKeyOfT>::const_iterator const_iterator;
		iterator begin()
		{
			return _t.begin();
		}
		iterator end()
		{
			return _t.end();
		}

		pair<iterator, bool>insert(const K& kv)
		{
			return _t.Insert(kv);
		}
	private:
		RBTree<K, K, SetKeyOfT>_t;
	};
}