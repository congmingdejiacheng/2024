//#include<iostream>
//#include<memory>//share指针的头文件；
//#include<exception>
#include<iostream>
#include<memory>
#include<list>
#include<thread>
#include<mutex>
using namespace std;
void Func()
{
	int* p1 = new int[10];
	int* p2 = nullptr;
	try
	{
		p2 = new int[20];
		try
		{
			int len, time;
			cin >> len >> time;
			throw("错误");
		}
		catch (...)
		{
			delete[] p1;
			cout << "delete:" << p1 << endl;

			delete[] p2;
			cout << "delete:" << p2 << endl;

			throw;  // 捕获什么抛出什么
		}
	}
	catch (...)
	{
		delete[] p1;
		cout << "delete:" << p1 << endl;

		throw;
	}

	delete[] p1;
	cout << "delete:" << p1 << endl;

	delete[] p2;
cout << "delete:" << p2 << endl;
}
template<class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr)
		:_ptr(ptr)
	{}
	~SmartPtr()
	{
		delete[] _ptr;
		cout << "delete:" << _ptr << endl;
	}
	T* get()
	{
		return _ptr;
	}
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	T& operator[](size_t i)
	{
		return _ptr[i];
	}

private:
	T* _ptr;
};
template<class T>
class auto_ptr
{
public:
	//构造函数
	auto_ptr(T* ptr == nullptr)
		:_ptr(ptr)
	{}
	//拷贝构造函数
	auto_ptr(auto_ptr<T>& ap)
		:_ptr(ap._ptr)
	{
		ap._ptr = nullptr;
	}
	//赋值函数
	auto_ptr<T>& operator=(auto_ptr<T>& ap)
	{
		if (_ptr != ap._ptr)//注意这里不可以自己
		{
			delete _ptr;//这里自己内容需要删除；
			_ptr = ap._ptr;
			ap._ptr = nullptr;
		}
		return *this;
	}
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}

	~auto_ptr()
	{
		delete _ptr;
	}
private:
	T* _ptr;
};
template<class T>
class shared_ptr
{
public:
	//构造函数
	shared_ptr(T* ptr = nullptr)
		:_ptr(ptr)
		, _pcount(new long int(1))
	{}
	//拷贝构造函数
	shared_ptr(shared_ptr<T>& sp)
		:_ptr(sp._ptr)
		, _pcount(sp._pcount)
	{
		++(*_pcount);
	}
	//赋值函数
	shareed_ptr<T>& operator=(shared_ptr<T>& sp)
	{
		if (_ptr == sp._ptr)
		{
			return *this;
		}
		//this的引用计数-1，并判断是否需要释放资源
		if（--（* _pcount) == 0)
		{
		delete _ptr;
		delete _pcount;
         }
		_ptr = sp._ptr;
		_pcount = sp._pcount;
		++(*_pcount);
		return *this;

	}
	int use_count()
	{
		return *_pcount;
	}
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	bool unique()//查看是否为重复；
	{
		retrun* _pcount == 1;
	}
	//析构函数
	~shared_ptr()
	{
		if (--(*_pcount) == 0)
		{
			delete _ptr;
			delete _pcount;
		}
	}
private:
	T* _ptr;//指向动态申请的指针
	long int*_pcount//引用计数
};
int main()
{
	SmartPtr<int> sp1(new int[10]);
	try
	{
		Func();
	}

	catch(const exception & e)//系统类
	{
	   cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "未知异常" << endl;
	}

	return 0;
}