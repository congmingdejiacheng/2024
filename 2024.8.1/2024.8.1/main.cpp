class Solution {
public:
    vector<int> twoSum(vector<int>& price, int target)
    {
        int left = 0;
        int right = price.size() - 1;
        while (left < right)
        {
            int sum = price[left] + price[right];
            if (sum > target)
            {
                right--;
            }
            else if (sum < target)
            {
                left++;
            }
            else
            {
                break;
            }
        }
        vector<int> arr;
        arr.push_back(price[right]);
        arr.push_back(price[left]);
        return arr;
    }
};
class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        int count = 0;

        for (int c = n - 1; c >= 0; c--)
        {
            int a = 0;
            int b = c - 1;
            while (a < b)
            {
                if (nums[a] + nums[b] > nums[c])
                {
                    count += (b - a);
                    b--;
                }
                else
                {
                    a++;
                }

            }

        }

        return count;
    }
};