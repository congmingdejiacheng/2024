#include <iostream>
#include <vector>
using namespace std;
//
//int main() {
//    int n = 0, m = 0, q = 0;
//    cin >> n >> m >> q;
//    vector<vector<int>>arr(n + 1, vector<int>(m + 1, 0));
//    int ans = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> arr[i][j];
//        }
//    }
//    vector<vector<int>>sum(n + 1, vector<int>(m + 1, 0));
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            sum[i][j] = sum[i - 1][j] + sum[i][j - 1] + arr[i][j] - sum[i - 1][j - 1];
//        }
//    }
//    while (q--)
//    {
//        int x1, x2, y1, y2;
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << sum[x2][y2] - sum[x2][y1 - 1] - sum[x1 - 1][y2] + sum[x1 - 1][y1 - 1] << endl;
//    }
//
//}
//// 64 λ������� printf("%lld")

    int pivotIndex(vector<int>& nums) {
        int n = nums.size();
        vector<int> sum1(n);
        vector<int> sum2(n);
        int ans = -1;
        for (int i = 0; i < n; i++)
        {
            if (i == 0)
            {
                sum1[i] = nums[i];
            }
            else
                sum1[i] = sum1[i - 1] + nums[i];
        }
        for (int j = n - 1; j >= 0; j--)
        {
            if (j == n - 1)
            {
                sum2[j] = nums[j];
            }
            else
                sum2[j] = sum2[j + 1] + nums[j];
        }
        for (int i = 0; i < n; i++)
        {
            if (sum1[i] == sum2[i])
            {
                ans = i;
                break;
            }
        }

        return ans;


    }

int main()
{
    vector<int> arr = { -1,-1,0,0,-1,-1 };
    cout<<pivotIndex(arr);
    return 0;
}