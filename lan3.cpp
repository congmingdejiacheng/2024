#include<bits/stdc++.h>
using namespace std;
const int N=20;
char s[N][N];
int dis[N][N];
int w,h;
int dx1[4]={-1,0,1,0};
int dy1[4]={0,1,0,-1};
int dfs(int x,int y)
{
    int ans=1;
    dis[x][y]=1;
    for(int i=0;i<4;i++)
    {
    	
        int a=x+dx1[i];
        int b=y+dy1[i];
        if(dis[a][b])
        continue;
        else if(s[a][b]!='.')
        continue;
        else if(a < 0 || a >= h || b < 0 || b >= w) 
        continue;
        else
        {
         
          ans+=dfs(a,b);
        }
        
    }
    return ans;
      
}
int main ()
{
    while(scanf("%d,%d",&w,&h),w||h)
    {
    for(int i=0;i<h;i++)
    cin>>s[i];
    int x,y;
    for(int i=0;i<h;i++)
    {
       for(int j=0;j<w;j++)
      {
        if(s[i][j]=='@')
        {
          x=i;
          y=j;
        }
      }

    }
    cout<<dfs(x,y)<<endl;
    }
    
    return 0;
}
