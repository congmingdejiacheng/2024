#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=55;
const int K=13;
const int C=14;
int arr[N][N];
int f[N][N][K][C];
int n,m,k;
int main ()
{
    cin>>n>>m>>k;
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=m;j++)
        {
            cin>>arr[i][j];
            arr[i][j]++;
        }
    }
    f[1][1][1][arr[1][1]]=1;
    f[1][1][0][0]=1;
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=m;j++)
        {
            if(i==1&&j==1)
            {
                continue;
            }
            for(int u=0;u<=k;u++)
            {
                for(int v=0;v<=13;v++)
                {
                    int& sub=f[i][j][u][v];
                    sub=(sub+f[i-1][j][u][v])%1000000007;
                    sub=(sub+f[i][j-1][u][v])%1000000007;
                    if(u>0&&v==arr[i][j])
                    {
                        for(int x=0;x<v;x++)
                        {
                             sub=(sub+f[i-1][j][u-1][x])%1000000007;
                             sub=(sub+f[i][j-1][u-1][x])%1000000007;
                        }
                       
                    }
                }
            }
        }
    }
    int res=0;
    for(int v=0;v<=13;v++)
    {
        res=(res+f[n][m][k][v])%1000000007;
    }
    cout<<res<<endl;
    return 0;
}
