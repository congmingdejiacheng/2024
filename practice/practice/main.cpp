class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        unordered_map<int, int> map;//这里记录下标；
        map[0] = -1;
        int Max = 0;
        for (auto& a : nums)
        {
            if (a == 0)
            {
                a = -1;
            }
        }
        int count = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            nums[i] += count;
            if (map.count(nums[i]))
            {
                Max = max(i - map[nums[i]], Max);
            }
            else
            {
                map[nums[i]] = i;
            }

            count = nums[i];
        }
        return Max;


    }
};
class Solution {
public:
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
        int m = mat.size();
        int n = mat[0].size();
        vector<vector<int>>answer(m, vector<int>(n, 0));
        vector<vector<int>>sum(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i < m + 1; i++)
        {
            for (int j = 1; j < n + 1; j++)
            {
                sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + mat[i - 1][j - 1];
            }

        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int i1 = max(i - k, 0) + 1;
                int j1 = max(j - k, 0) + 1;
                int i2 = min(i + k, m - 1) + 1;
                int j2 = min(j + k, n - 1) + 1;
                answer[i][j] = sum[i2][j2] - sum[i1 - 1][j2] - sum[i2][j1 - 1] + sum[i1 - 1][j1 - 1];
            }
        }
        return answer;



    }
};