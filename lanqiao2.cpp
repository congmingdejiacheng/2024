#include <iostream>
#include <vector>

int maxMissilesToIntercept(const std::vector<int>& missileHeights) {
    int currentHeight = 0;
    int interceptCount = 0;

    for (int height : missileHeights) {
        if (height <= currentHeight) {
            // 如果新高度小于等于当前高度，不能拦截
        } else {
            // 如果新高度大于当前高度，可以拦截，更新当前高度和拦截数
            currentHeight = height;
            interceptCount++;
        }
    }

    return interceptCount;
}

int main() {
    std::vector<int> missileHeights; // 从输入读取导弹高度
    // 读取导弹高度并存储在missileHeights中

    int maxIntercept = maxMissilesToIntercept(missileHeights);
    std::cout << "这套系统最多能拦截 " << maxIntercept << " 枚导弹。" << std::endl;

    return 0;
}
