#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2)
    {
        int arr1[1001];
        int arr2[1001];
        vector<int> arr3;
        for (auto x : nums1)
        {
            arr1[x]++;
        }
        for (auto y : nums2)
        {
            arr2[y]++;
        }
        for (int i = 0; i <= 1000; i++)
        {
            if (arr1[i] && arr2[i])
            {
                int a = min(arr1[i], arr2[i]);
                for (int j = 1; j <= a; j++)
                {
                    arr3.push_back(i);
                }
            }
        }
        return arr3;


    }
};