class Solution {
public:
    int longestOnes(vector<int>& nums, int k) {
        int right = 0;
        int left = 0;
        int n = nums.size();
        int zero = 0;
        int len = 0;
        for (; right < n; right++)
        {
            if (nums[right] == 0)
                zero++;
            while (zero > k)
            {
                if (nums[left] == 0)
                    zero--;
                left++;
            }
            len = max(len, right - left + 1);
        }
        return len;
    }
};
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int left = 0;
        int right = 0;
        int len = 0;
        unordered_map<char, int> s1;
        int n = s.size();
        for (; right < n; right++)
        {
            s1[s[right]]++;
            while (s1[s[right]] > 1)
            {
                s1[s[left]]--;
                left++;
            }
            len = max(len, right - left + 1);

        }
        return len;

    }
};