#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<unordered_map>
    vector<int> findSubstring(string s, vector<string>& words) {
        int n1 = words[0].size();
        int n2 = words.size();
        int n = s.size();
        int right = 0, left = 0;
        unordered_map<string, int> m1;

        vector<int> arr;
        for (auto x : words)
        {
            m1[x]++;
        }
        for (int i = 0; i < n1; i++)
        {
            unordered_map<string, int> m2;
            right = i;
            left = i;
            int count = 0;
            for (; right <= n - n1; right += n1)
            {
                string s1 = s.substr(right, n1);
                m2[s1]++;
                if (m2[s1] <= m1[s1])
                {
                    count++;
                }
                while (right - left + 1 > n2 * n1)
                {
                    string s3 = s.substr(left, n1);
                    if (m2[s3] < m1[s3])
                    {
                        count--;
                    }
                    m2[s3]--;
                    if (m2[s3] == 0)
                        m2.erase(s3);
                    left += n1;
                }
                if (count == n2)
                {
                    arr.push_back(left);
                }

            }
        }
        return arr;
    }
    int main()
    {
        vector<string> word = { "bar","foo","the" };
        findSubstring("barfoofoobarthefoobarman",word);
        return 0;
    }
