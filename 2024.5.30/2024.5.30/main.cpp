//#define _CRT_SECURE_NO_WARNINGS 1
// 力扣150
////方法一
//class Solution {
//public:
//    int evalRPN(vector<string>& tokens)
//    {
//        stack<int>st;
//        //map<string,function<int(int ,int)>>opFuncMap=
//        map<string, function<int(int, int)>> opFuncMap =
//        {
//           {"+",[](int x,int y) {return x + y; }},
//           {"-",[](int x,int y) {return x - y; }},
//           {"*",[](int x,int y) {return x * y; }},
//           {"/",[](int x,int y) {return x / y; }},
//        };
//        for (auto& str : tokens)
//        {
//            if (opFuncMap.find(str) == opFuncMap.end())
//            {
//                st.push(stoi(str));
//            }
//            else
//            {
//                int right = st.top();
//                st.pop();
//                int left = st.top();
//                st.pop();
//                st.push(opFuncMap[str](left, right));
//
//            }
//
//
//        }
//
//        return st.top();
//    }
//};
////方法二
//class Solution {
//public:
//    int evalRPN(vector<string>& tokens) {
//        stack<int>st;
//        for (auto& str : tokens)
//        {
//            if (str == "+" || str == "-" || str == "*" || str == "/")
//            {
//                int right = st.top();
//                st.pop();
//                int left = st.top();
//                st.pop();
//                switch (str[0])
//                {
//                case '+':
//                    st.push(left + right);
//                    break;
//                case '-':
//                    st.push(left - right);
//                    break;
//                case '*':
//                    st.push(left * right);
//                    break;
//                case '/':
//                    st.push(left / right);
//                    break;
//                default:
//                    break;
//
//                }
//
//            }
//            else
//            {
//                st.push(stoi(str));
//            }
//        }
//        return st.top();
//    }
//};