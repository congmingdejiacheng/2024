class Solution {
public:
    int f(int c)
    {
        int count = 0;
        while (c)
        {
            count += (c % 10) * (c % 10);
            c /= 10;
        }
        return count;
    }
    bool isHappy(int n) {
        int cur = n;
        int prev = n;
        while (cur != prev || (cur == n && prev == n))
        {
            cur = f(cur);
            prev = f(f(prev));
        }
        if (cur == 1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

};