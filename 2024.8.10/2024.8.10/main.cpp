class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        int n = nums.size();
        vector<int> arr;
        int left = 0, right = n - 1;
        if (n == 0)
            return vector<int>() = { -1,-1 };

        while (left < right)
        {
            int mid = (left + right) / 2;
            if (nums[mid] >= target)
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }
        }
        if (nums[left] == target)
            arr.push_back(left);
        else
            arr.push_back(-1);
        left = 0;
        right = n - 1;
        while (left < right)
        {
            int mid = (right + left + 1) / 2;
            if (nums[mid] <= target)
            {
                left = mid;
            }
            else
            {
                right = mid - 1;
            }
        }
        if (nums[left] == target)
            arr.push_back(left);
        else
            arr.push_back(-1);
        return arr;

    }
};